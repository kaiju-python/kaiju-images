from .services import *

__version__ = '2.0.1'
__python_version__ = '3.8'
__author__ = 'antonnidhoggr@me.com'
__service_package__ = True
